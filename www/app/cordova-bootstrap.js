
function onDeviceReady(root){
    console.log('App bound to cordova!');

    var requireref = document.createElement("script");
    requireref.setAttribute("type", "text/javascript");
    requireref.setAttribute("src", "lib/requirejs/require.js");
    requireref.setAttribute("data-main", "app/mainConfig.js");
    document.getElementsByTagName("head")[0].appendChild(requireref);
}


if(document.location.protocol == "file:"){
    var root = "file:///android_asset/www";

    var cordovaref = document.createElement("script");
    cordovaref.setAttribute("type", "text/javascript");
    cordovaref.setAttribute("src", "cordova.js");
    document.getElementsByTagName("head")[0].appendChild(cordovaref);

    document.addEventListener("deviceready", function(){onDeviceReady(root);});
}else{
    var root = "../";
    onDeviceReady(root);
}