define([
    'angular',
    'ionic',
    'angular-ionic',
    'angularAnimate',
    'angularUiRouter',
    'famo',
    './index'
], function(ng){
    'use strict';

    /*
    * Replace eventmo everywhere you see it with your app name.
     */
    return ng.module('eventmo', [
        'eventmo.modules',
        'famous.angular',
        'ionic',
        'ui.router',
        'ngAnimate'
    ]);
});